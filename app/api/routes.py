import logging

from fastapi import APIRouter, WebSocket
from .config import get_settings
from .models import Event, Origin


router = APIRouter()

responses = {
    404: {"description": "Not found"},
}


@router.post(
    "/event",
    status_code=200,
    response_model=bool,
    responses={**responses},
    tags=["communication"],
    summary="Send an event to this component",
)
async def receive_event(event: Event):
    logging.info(event)
    return True


@router.get(
    "/info",
    status_code=200,
    response_model=Origin,
    tags=["information"],
    summary="Information about this component",
)
async def information():
    answer = Origin(
        name=get_settings().pod_name,
        namespace=get_settings().pod_namespace,
        uid=get_settings().pod_uid,
        pod_ip=get_settings().pod_ip,
        node_name=get_settings().node_name,
        node_ip=get_settings().node_ip,
    )
    logging.info(f"Answering with: {answer}")
    return answer
