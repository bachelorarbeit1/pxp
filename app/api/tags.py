tags_metadata = [
    {
        "name": "communication",
        "description": "Operations regarding communication",
    },
    {
        "name": "information",
        "description": "Operations regarding information capabilities",
    },
]
