import os

from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    pod_name: str = "PXP"
    pod_namespace: str
    pod_uid: str
    pod_ip: str
    node_name: str
    node_ip: str


@lru_cache()
def get_settings():
    return Settings()
