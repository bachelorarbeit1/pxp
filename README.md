# Policy Execution Point (PXP)

This is a repository of a Policy Execution Point (PXP) dummy writen in Python using FastAPI.

## Usage
The pxp creates a http-server. In order for this to work a few environment variables need to be set up beforehand. This repository assumes a deployment via Kubernetes. A lot of the listed environment variables can be easily injected by Kubernetes via the downward-api.

| Name | Description |
| ---      |  ------  |
| POD_NAME   | name of the pod running the pxp  |
| POD_NAMESPACE | namespace of the pod running the pxp |
| POD_UID | an unique id of the pod running the pxp |
| POD_IP | the ip of the pod running the pxp |
| NODE_NAME | name of the node running the pod |
| NODE_IP | ip of the node running the pod|

### Run the docker image

For running the Docker image you need to pass in the environment variables. This can be done via:

`-e` Parameter in the docker run command:

`docker run -e POD_NAME='pxp' -e <...> -p <host-port>:<container-port> --name pxp nschuler/thesis:pxp`

an env-file:

`docker run --env-file ./.env -p <host-port>:<container-port> --name pxp nschuler/thesis:pxp`

In addition to the forementioned variables you can and should set the following variables:

| Name | Description |
| ---      |  ------  |
| PORT   | defines the port which gunicorn will be listening to  |
| WORKER | defines how many processes gunicorn should start |


## Communication
The routes for communicating with the pxp are as follows:

| Route | Description |
| ---      |  ------  |
| /event   | for sending an event to the pxp  |
| /info | get information about this pxp |
| /docs | show open-api using swagger ui |
| /redoc | show open-api using ReDoc |


## Other
This Repository uses the gitlab-ci to:
- check compliance with the black code style
- create and deploy a docker image to dockerhub

If your planning to clone the repository you need to set the following Variables in order to get the CI working correctly:

| Name | Description |
| ---      |  ------  |
| CI_REGISTRY_TOKEN   | registry Token from dockerhub for pushing the image |
| DOCKER_ACCOUNT | name of the Account |
| DOCKER_IMAGE | name of the image |
| DOCKER_TAG | tag for the image |

**Do not forget to mask the CI_REGISTRY_TOKEN or everyone could push images to your account by looking at the CI logs**
