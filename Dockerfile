FROM python:3.9-slim-buster AS compile-image

LABEL maintainer="Nicolas Schuler"
LABEL email="n-schuler@gmx.de"
LABEL description="A pxp dummy"

# Steps for compiling dependencies
WORKDIR /install
COPY requirements.txt .
RUN apt-get update && \
    apt-get install -y build-essential
RUN pip install --prefix=/install wheel && \
    pip install --prefix=/install -r requirements.txt

# Steps to create build-image
FROM python:3.9-slim-buster AS build-image

# For new non-root user
ENV USER=dummy
ENV GID=1000
ENV UID=1000

# Application Variables
ENV PORT=8000
ENV WORKER=2
ENV WORKER_TYPE="uvicorn.workers.UvicornWorker"

RUN groupadd -g $GID -o $USER && \
    adduser --disabled-password \
    --uid $UID --gid $GID --gecos "" \
    $USER
USER $USER
COPY --from=compile-image /install /usr/local
WORKDIR /home/$USER/src
COPY . .

EXPOSE $PORT
CMD exec gunicorn -w $WORKER -k $WORKER_TYPE -b 0.0.0.0:$PORT main:app
